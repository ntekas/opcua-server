# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "/home/atlas/ntekas/opcua-server/build/open62541-compat"
  "/home/atlas/ntekas/opcua-server/build/FrameworkInternals/EnabledModules/open62541-compat_download/src/open62541-compat-build"
  "/home/atlas/ntekas/opcua-server/build/FrameworkInternals/EnabledModules/open62541-compat_download"
  "/home/atlas/ntekas/opcua-server/build/FrameworkInternals/EnabledModules/open62541-compat_download/tmp"
  "/home/atlas/ntekas/opcua-server/build/FrameworkInternals/EnabledModules/open62541-compat_download/src/open62541-compat-stamp"
  "/home/atlas/ntekas/opcua-server/build/FrameworkInternals/EnabledModules/open62541-compat_download/src"
  "/home/atlas/ntekas/opcua-server/build/FrameworkInternals/EnabledModules/open62541-compat_download/src/open62541-compat-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "/home/atlas/ntekas/opcua-server/build/FrameworkInternals/EnabledModules/open62541-compat_download/src/open62541-compat-stamp/${subDir}")
endforeach()
if(cfgdir)
  file(MAKE_DIRECTORY "/home/atlas/ntekas/opcua-server/build/FrameworkInternals/EnabledModules/open62541-compat_download/src/open62541-compat-stamp${cfgdir}") # cfgdir has leading slash
endif()
