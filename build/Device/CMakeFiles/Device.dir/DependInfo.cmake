
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/atlas/ntekas/opcua-server/build/Device/generated/Base_All.cpp" "Device/CMakeFiles/Device.dir/generated/Base_All.cpp.o" "gcc" "Device/CMakeFiles/Device.dir/generated/Base_All.cpp.o.d"
  "/home/atlas/ntekas/opcua-server/Device/src/DMotor.cpp" "Device/CMakeFiles/Device.dir/src/DMotor.cpp.o" "gcc" "Device/CMakeFiles/Device.dir/src/DMotor.cpp.o.d"
  "/home/atlas/ntekas/opcua-server/build/Device/src/DRoot.cpp" "Device/CMakeFiles/Device.dir/src/DRoot.cpp.o" "gcc" "Device/CMakeFiles/Device.dir/src/DRoot.cpp.o.d"
  )

# Pairs of files generated by the same build rule.
set(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/home/atlas/ntekas/opcua-server/build/Device/src/DRoot.cpp" "/home/atlas/ntekas/opcua-server/build/Device/include/DRoot.h"
  )


# Targets to which this target links which contain Fortran sources.
set(CMAKE_Fortran_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
