
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.

    This file was generated by quasar (https://github.com/quasar-team/quasar/)

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.

 */



#ifndef DROOT_H_
#define DROOT_H_

#include <opcua_platformdefs.h>

#include <vector>
#include <string>

namespace Device
{

class DMotor;

class DRoot
{
public:
    /* yes, it's a singleton */
    static DRoot* getInstance ();

    DRoot ();
    DRoot (const DRoot& other) = delete;
    DRoot& operator= (const DRoot& other) = delete;

    virtual ~DRoot ();

    /* To gracefully quit */
    void unlinkAllChildren () const;

    /* For constructing the tree of devices and for browsing children. */
    void add (DMotor* device);
    const std::vector<DMotor* >& motors () const;


    /* find methods for children */


    /* query address-space for full name (mostly for debug purposes) */
    std::string getFullName () const {
        return "[ROOT]";
    }

private:
    static DRoot *m_instance; /* it's a singleton class */

    /* Collections of device logic children objects */
    std::vector<DMotor* > m_Motors;


};

}
#endif // include guard