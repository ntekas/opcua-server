/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.

    This file was generated by quasar (https://github.com/quasar-team/quasar/)

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.

 */


#include <ASUtils.h>
#include <ASInformationModel.h>
#include <ASNodeQueries.h>
#include <ASCommon.h>

#include <DRoot.h>

#include <Configurator.h>
#include <Configuration.hxx>

#include <CalculatedVariablesEngine.h>
#include <FreeVariablesEngine.h>

#include <meta.h>

#include <LogIt.h>
#include <LogLevels.h>

#include <Utils.h>

// includes for AS classes and Device classes
#include <ASMotor.h>
#include <DMotor.h>

[[maybe_unused]] static std::string mapSingleItemNodeFullName(std::string input)
{
    size_t singleItemNodePrefixBegins = input.find(AddressSpace::SingleItemNodeObjectPrefix);
    input.replace(
        singleItemNodePrefixBegins,
        AddressSpace::SingleItemNodeObjectPrefix.length(),
        "");
    return input;
}






// forward declare configure function signatures

Device::DMotor*
configureMotor(
    const Configuration::Motor& config,
    AddressSpace::ASNodeManager *nm,
    UaNodeId parentNodeId
    , Device::Parent_DMotor* parentDevice
)
;

// configure function bodies

Device::DMotor*
configureMotor(
    const Configuration::Motor& config,
    AddressSpace::ASNodeManager *nm,
    UaNodeId parentNodeId
    , Device::Parent_DMotor* parentDevice
)
{
    // instantiate address space side object
    AddressSpace::ASMotor *asItem = new AddressSpace::ASMotor(
        parentNodeId,
        nm->getTypeNodeId(AddressSpace::ASInformationModel::AS_TYPE_MOTOR),
        nm,
        config
    );

    // instantiate device side object
    Device::DMotor *dItem = new Device::DMotor(config, parentDevice);

    // link address space side and device side objects
    asItem->linkDevice( dItem );
    dItem->linkAddressSpace( asItem, asItem->nodeId().toString().toUtf8() );

    // process each 'instantiated by design' instance individually - order not important

    // process each 'instatiated by config' as XML nodes - order significant (calc'd vars)

    validateContentOrder(config, config.CalculatedVariable(), Configuration::Motor::CalculatedVariable_id);


    // configure child nodes - content_order retains order from configuration XMl file
    for(const auto& orderedIter : config.content_order())
    {
        const auto xmlIndex = orderedIter.index;
        const auto xmlType = orderedIter.id;
        switch(xmlType)
        {
        case Configuration::Motor::CalculatedVariable_id:
        {
            const auto& xmlObj(config.CalculatedVariable()[xmlIndex]);
            LOG(Log::DBG)<<__FUNCTION__<<" Configuring type [id:"<<xmlType<<", nm:CalculatedVariable] ordering index ["<<xmlIndex<<"]";
            CalculatedVariables::Engine::instantiateCalculatedVariable(nm, asItem->nodeId(), xmlObj);
        }
        break;
        case Configuration::Motor::FreeVariable_id:
        {
            const auto& freeVariableXmlElement(config.FreeVariable()[xmlIndex]);
            AddressSpace::FreeVariablesEngine::instantiateFreeVariable(nm, asItem->nodeId(), freeVariableXmlElement);
        }
        break;
        default:
            LOG(Log::DBG)<<__FUNCTION__<<" Ignoring type [id:"<<xmlType<<"] ordering index ["<<xmlIndex<<"]"; // valid to ignore, StandardMeta etc
            break;
        }
    }


    return dItem;
}

bool runConfigurationDecoration(Configuration::Configuration& theConfiguration, ConfigXmlDecoratorFunction& configXmlDecoratorFunction)
{
    if(!configXmlDecoratorFunction) return true;

    if(configXmlDecoratorFunction(theConfiguration))
    {
        return true;
    }
    else
    {
        std::cout << __FUNCTION__ << " Error: device specific configuration decoration failed, check logs for details" << std::endl;
    }
    return false;
}

std::unique_ptr<Configuration::Configuration> loadConfigurationFromFile(const std::string& fileName)
{
    try
    {
        return std::unique_ptr<Configuration::Configuration>(Configuration::configuration(fileName, ::xml_schema::flags::keep_dom));
    }
    catch (xsd::cxx::tree::parsing<char>& exception)
    {
        LOG(Log::ERR) << __FUNCTION__ << " Configuration: Failed when trying to open the configuration, with general error message: " << exception.what();
        for( const auto& error : exception.diagnostics() )
        {
            LOG(Log::ERR) << __FUNCTION__ << "Configuration: Problem at " << error.id() <<":" << error.line() << ": " << error.message();
        }
        throw std::runtime_error("Configuration: failed to load configuration file ["+fileName+"]. The exact problem description should have been logged.");
    }
}

bool configure (std::string fileName, AddressSpace::ASNodeManager *nm, ConfigXmlDecoratorFunction configXmlDecoratorFunction)
{
    std::unique_ptr<Configuration::Configuration> theConfiguration = loadConfigurationFromFile(fileName);

    CalculatedVariables::Engine::loadGenericFormulas(theConfiguration->CalculatedVariableGenericFormula());

    UaNodeId asRootNodeId = UaNodeId(OpcUaId_ObjectsFolder, 0);
    Device::DRoot *dRoot = Device::DRoot::getInstance();
    (void)dRoot; // silence-out the warning from unused variable

    configureMeta( *theConfiguration.get(), nm, asRootNodeId );
    if(!runConfigurationDecoration(*theConfiguration, configXmlDecoratorFunction)) return false;

    const Configuration::Configuration& config = *theConfiguration;

    // process each 'instantiated by design' instance individually - order not important

    // process each 'instatiated by config' as XML nodes - order significant (calc'd vars)

    validateContentOrder(config, config.Motor(), Configuration::Configuration::Motor_id);
    validateContentOrder(config, config.CalculatedVariable(), Configuration::Configuration::CalculatedVariable_id);


    // configure child nodes - content_order retains order from configuration XMl file
    for(const auto& orderedIter : config.content_order())
    {
        const auto xmlIndex = orderedIter.index;
        const auto xmlType = orderedIter.id;
        switch(xmlType)
        {
        case Configuration::Configuration::Motor_id:
        {
            const auto& xmlObj(config.Motor()[xmlIndex]);
            LOG(Log::DBG)<<__FUNCTION__<<" Configuring class type [id:"<<xmlType<<", nm:Motor] ordering index ["<<xmlIndex<<"] parent class type []";
            // class [Motor] has device logic: configure device object
            Device::Parent_DMotor* pInnerItemParent = dRoot;
            auto dInnerObj = configureMotor(xmlObj, nm, asRootNodeId, pInnerItemParent);

            // register class Motor] with parent (or register orphan)
            dRoot->add(dInnerObj);
        }
        break;
        case Configuration::Configuration::CalculatedVariable_id:
        {
            const auto& xmlObj(config.CalculatedVariable()[xmlIndex]);
            LOG(Log::DBG)<<__FUNCTION__<<" Configuring type [id:"<<xmlType<<", nm:CalculatedVariable] ordering index ["<<xmlIndex<<"]";
            CalculatedVariables::Engine::instantiateCalculatedVariable(nm, asRootNodeId, xmlObj);
        }
        break;
        case Configuration::Configuration::FreeVariable_id:
        {
            const auto& freeVariableXmlElement(config.FreeVariable()[xmlIndex]);
            AddressSpace::FreeVariablesEngine::instantiateFreeVariable(nm, asRootNodeId, freeVariableXmlElement);
        }
        break;
        default:
            LOG(Log::DBG)<<__FUNCTION__<<" Ignoring type [id:"<<xmlType<<"] ordering index ["<<xmlIndex<<"]"; // valid to ignore, StandardMeta etc
            break;
        }
    }


    return true;
}

void unlinkAllDevices (AddressSpace::ASNodeManager *nm)
{
    unsigned int totalObjectsNumber = 0;
    {
        std::vector<AddressSpace::ASMotor*> objects;
        AddressSpace::findAllObjectsByPatternInNodeManager<AddressSpace::ASMotor>(nm, ".*", objects);
        totalObjectsNumber += objects.size();
        for(auto a : objects)
        {
            a->unlinkDevice();
        }
    }
    LOG(Log::INF) << __FUNCTION__ << " total number of unlinked objects: " << totalObjectsNumber;
}